package pl.sarelo.androidcodingchallenge.core.di

import androidx.room.Room
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pl.sarelo.androidcodingchallenge.BuildConfig
import pl.sarelo.androidcodingchallenge.core.async.SchedulerProvider
import pl.sarelo.androidcodingchallenge.core.db.AccDatabase
import pl.sarelo.androidcodingchallenge.core.network.api.ApiFactory
import pl.sarelo.androidcodingchallenge.core.network.api.HttpClientProvider
import pl.sarelo.androidcodingchallenge.core.network.api.NetworkApiFactory
import pl.sarelo.androidcodingchallenge.core.network.repositories.NetworkImageRepository
import pl.sarelo.androidcodingchallenge.core.network.requests.ApiImageRequest
import pl.sarelo.androidcodingchallenge.domain.repositories.ImageRepository
import pl.sarelo.androidcodingchallenge.domain.usecases.GetImagesUseCase
import pl.sarelo.androidcodingchallenge.feature.home.MainActivityViewModel

private const val API_URL = "https://picsum.photos"

val appModule = module {

    single<SchedulerProvider> {
        object : SchedulerProvider {
            override fun io() = Schedulers.io()
            override fun mainThread() = AndroidSchedulers.mainThread()
        }
    }

    single { HttpClientProvider(BuildConfig.DEBUG).buildOkHttpClient() }

    single<ApiFactory> { NetworkApiFactory(API_URL, get()) }

    single { get<ApiFactory>().createRequest(ApiImageRequest::class.java) }

    single<ImageRepository> { NetworkImageRepository(get()) }

    single {
        Room.databaseBuilder(androidContext(), AccDatabase::class.java, AccDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    single { get<AccDatabase>().databaseDao() }

    single { GetImagesUseCase(get(), get(), get()) }

    viewModel { MainActivityViewModel(get()) }

}
