package pl.sarelo.androidcodingchallenge

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class AccAppGlideModule : AppGlideModule()