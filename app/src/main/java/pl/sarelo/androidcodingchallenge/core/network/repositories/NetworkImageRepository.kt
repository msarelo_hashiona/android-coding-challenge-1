package pl.sarelo.androidcodingchallenge.core.network.repositories

import io.reactivex.Single
import pl.sarelo.androidcodingchallenge.core.network.requests.ApiImageRequest
import pl.sarelo.androidcodingchallenge.domain.entities.ImageRepositoryResult
import pl.sarelo.androidcodingchallenge.domain.repositories.ImageRepository

class NetworkImageRepository(private val apiImageRequest: ApiImageRequest) : ImageRepository {

    override fun getImages(pageNumber: Int, itemsPerPage: Int): Single<ImageRepositoryResult> {
        val queryMap = mutableMapOf(
            "page" to pageNumber.toString(),
            "limit" to itemsPerPage.toString()
        )
        return apiImageRequest.fetchImages(queryMap)
            .map {
                ImageRepositoryResult.Success(it) as ImageRepositoryResult
            }
            .onErrorReturn {
                ImageRepositoryResult.NetworkError(it)
            }
    }
}
