package pl.sarelo.androidcodingchallenge.feature.home.grid

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class LimitedScrollListener(
    private val linearLayoutManager: LinearLayoutManager,
    private val maxItemsPerPage: Int
) :
    RecyclerView.OnScrollListener() {

    private var currentPage = 1

    abstract fun onLoadMore()

    fun reset() {
        currentPage = 1
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (hasReachedEdge(linearLayoutManager)) {
            currentPage++
            onLoadMore()
        }
    }

    private fun hasReachedEdge(linearLayoutManager: LinearLayoutManager): Boolean {
        with(linearLayoutManager) {
            val visibleItemCount = childCount
            val totalItemCount = itemCount
            val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()

            return (visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                    firstVisibleItemPosition >= 0 &&
                    totalItemCount >= maxItemsPerPage)
        }
    }
}
