package pl.sarelo.androidcodingchallenge.core.network.api

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class HttpClientProvider(debug: Boolean) {

    private val loggingLevel = getLoggingLevel(debug)

    private fun getLoggingLevel(debug: Boolean): HttpLoggingInterceptor.Level {
        return if (debug) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
    }

    fun buildOkHttpClient(): OkHttpClient = buildBaseClient()
        .addInterceptor(buildHttpLoggingInterceptor())
        .build()

    private fun buildBaseClient() = OkHttpClient()
        .newBuilder()

    private fun buildHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Log.d("NETOWRK", "OkHttp: $it")
        })
        loggingInterceptor.level = loggingLevel
        return loggingInterceptor
    }
}
