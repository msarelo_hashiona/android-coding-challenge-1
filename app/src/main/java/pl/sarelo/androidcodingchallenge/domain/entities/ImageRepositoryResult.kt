package pl.sarelo.androidcodingchallenge.domain.entities

import pl.sarelo.androidcodingchallenge.core.network.model.ApiImageModel

sealed class ImageRepositoryResult {
    class Success(val data: List<ApiImageModel> = emptyList()) : ImageRepositoryResult()

    class NetworkError(val error: Throwable) : ImageRepositoryResult()
}