package pl.sarelo.androidcodingchallenge.core.network.repositories

import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import pl.sarelo.androidcodingchallenge.core.network.model.ApiImageModel
import pl.sarelo.androidcodingchallenge.core.network.requests.ApiImageRequest
import pl.sarelo.androidcodingchallenge.domain.entities.ImageRepositoryResult

class NetworkMusicRepositoryTest {

    companion object {
        private lateinit var apiImageRequest: ApiImageRequest

        @BeforeClass
        @JvmStatic
        fun setup() {
            apiImageRequest = mockk()
        }
    }

    private lateinit var observer: TestObserver<ImageRepositoryResult>

    @Before
    fun beforeEachTest() {
        observer = TestObserver()
    }

    @Test
    fun `fetchImages() should return Success with data when API return items`() {
        // Given
        val mockedResponse =
            (1..10).asSequence().map { mockk<ApiImageModel>(relaxed = true) }.toList()
        every {
            apiImageRequest.fetchImages(any())
        } returns Single.just(mockedResponse)

        val networkMusicRepository = NetworkImageRepository(apiImageRequest)

        // When
        networkMusicRepository.getImages(0, 0).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            Assert.assertEquals(
                "Returns Success",
                ImageRepositoryResult.Success::class,
                it.values().first()::class
            )
            Assert.assertEquals(
                "Size of result should be the same",
                10,
                (it.values().first() as ImageRepositoryResult.Success).data.size
            )
        }
    }

    @Test
    fun `fetchImages() should return NetworkError when API throw exception`() {
        // Given
        every {
            apiImageRequest.fetchImages(any())
        } returns Single.error(NullPointerException("Null"))

        val networkMusicRepository = NetworkImageRepository(apiImageRequest)

        // When
        networkMusicRepository.getImages(0, 0).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            Assert.assertEquals(
                "Returns NetworkError",
                ImageRepositoryResult.NetworkError::class,
                it.values().first()::class
            )
        }
    }
}

