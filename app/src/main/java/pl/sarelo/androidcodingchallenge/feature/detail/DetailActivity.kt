package pl.sarelo.androidcodingchallenge.feature.detail

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import pl.sarelo.androidcodingchallenge.BR
import pl.sarelo.androidcodingchallenge.R
import pl.sarelo.androidcodingchallenge.databinding.DetialActivityBinding

class DetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_IMAGE_TRANSITION_NAME: String = "EXTRA_IMAGE_TRANSITION_NAME"
        const val AUTHOR_NAME = "AUTHOR_NAME"
        const val IMAGE_ID = "IMAGE_ID"
        const val IMAGE_URL = "IMAGE_URL"
        private const val TYPE = "text/plain"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: DetialActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.detial_activity)

        val imageTransitionName = intent.extras?.getString(EXTRA_IMAGE_TRANSITION_NAME)
        binding.image.transitionName = imageTransitionName;

        binding.setVariable(BR.imageId, intent.extras?.getInt(IMAGE_ID))

        binding.authorName.text = intent.extras?.getString(AUTHOR_NAME).orEmpty()
        binding.shareButton.setOnClickListener {
            val shareIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, intent.extras?.getString(IMAGE_URL).orEmpty())
                type = TYPE
            }
            startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.share)))
        }


    }

    override fun onBackPressed() {
        supportFinishAfterTransition()
    }
}
