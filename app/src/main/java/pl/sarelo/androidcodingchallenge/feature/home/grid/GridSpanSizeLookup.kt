package pl.sarelo.androidcodingchallenge.feature.home.grid

import androidx.recyclerview.widget.GridLayoutManager

class GridSpanSizeLookup(private val isSmartphone: Boolean, private val isLandscape: Boolean) :
    GridLayoutManager.SpanSizeLookup() {

    companion object {
        const val PORTRAIT_SPAN_COUNT = 2
        const val LANDSCAPE_SPAN_COUNT = 4

        const val SMARTPHONE_FACTOR = 1
        const val TABLET_FACTOR = 2

        const val MAX_SPAN_COUNT = TABLET_FACTOR * LANDSCAPE_SPAN_COUNT
    }

    override fun getSpanSize(position: Int): Int {
        val spanCount = if (isLandscape) LANDSCAPE_SPAN_COUNT else PORTRAIT_SPAN_COUNT
        val deviceFactor = if (isSmartphone) SMARTPHONE_FACTOR else TABLET_FACTOR
        return MAX_SPAN_COUNT / (spanCount * deviceFactor)
    }
}
