package pl.sarelo.androidcodingchallenge.feature.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel
import pl.sarelo.androidcodingchallenge.R
import pl.sarelo.androidcodingchallenge.databinding.ActivityMainBinding
import pl.sarelo.androidcodingchallenge.domain.entities.Image
import pl.sarelo.androidcodingchallenge.feature.detail.DetailActivity
import pl.sarelo.androidcodingchallenge.feature.home.grid.GridAdapter
import pl.sarelo.androidcodingchallenge.feature.home.grid.GridSpanSizeLookup
import pl.sarelo.androidcodingchallenge.feature.home.grid.LimitedScrollListener


class MainActivity : AppCompatActivity() {

    private val mainActivityViewModel: MainActivityViewModel by viewModel()

    private var binding: ActivityMainBinding? = null

    private lateinit var imageGridAdapter: GridAdapter
    private lateinit var limitedScrollListener: LimitedScrollListener

    private fun Context?.isSmartphone() = this?.resources?.getBoolean(R.bool.isSmartphone) == true
    private fun Context?.isLandscape() = this?.resources?.getBoolean(R.bool.isLandscape) == true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivityViewModel.loadData()

        imageGridAdapter = createGridAdapter()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding?.apply {
            lifecycleOwner = this@MainActivity
            viewModel = mainActivityViewModel

            mainActivityViewModel.error.observe(this@MainActivity, {
                Snackbar.make(this.root, "Error: ${it.message}", Snackbar.LENGTH_SHORT).show()
            })
        }

        mainActivityViewModel.images.observe(this, {
            imageGridAdapter.addItems(it)
        })

        initRecycler()
    }

    private fun createGridAdapter(): GridAdapter {
        return GridAdapter { image: Image, imageView: View ->

            ViewCompat.setTransitionName(imageView, image.url);
            val transitionName = ViewCompat.getTransitionName(imageView).orEmpty()
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(DetailActivity.AUTHOR_NAME, image.author)
            intent.putExtra(DetailActivity.IMAGE_ID, image.id)
            intent.putExtra(DetailActivity.IMAGE_URL, image.url)
            intent.putExtra(DetailActivity.EXTRA_IMAGE_TRANSITION_NAME, transitionName);

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this@MainActivity,
                imageView,
                transitionName
            )
            startActivity(intent, options.toBundle())
        }
    }

    private fun initRecycler() {
        binding?.images?.let {
            with(it) {

                val manager =
                    GridLayoutManager(this@MainActivity, GridSpanSizeLookup.MAX_SPAN_COUNT).apply {
                        spanSizeLookup = GridSpanSizeLookup(isSmartphone(), isLandscape())
                    }

                layoutManager = manager
                adapter = imageGridAdapter


                clearOnScrollListeners()
                limitedScrollListener =
                    object : LimitedScrollListener(manager, MainActivityViewModel.ITEMS_PER_PAGE) {
                        override fun onLoadMore() {
                            mainActivityViewModel.loadMoreData()
                        }
                    }
                addOnScrollListener(limitedScrollListener)
            }
        }
    }

}